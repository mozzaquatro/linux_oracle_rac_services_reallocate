#!/bin/bash
# reloca o serviÃ§o em sua instancia preferencial se a mesma estiver disponivel
#
ORACLE_BASE=/u01/app/oracle; export ORACLE_BASE
GRID_HOME=/u01/app/11.2.0/grid_1; export GRID_HOME
DB_HOME=$ORACLE_BASE/product/11.2.0/dbhome_2; export DB_HOME
ORACLE_HOME=$DB_HOME; export ORACLE_HOME
ORACLE_SID=database_name; export ORACLE_SID
BASE_PATH=/usr/sbin:$PATH; export BASE_PATH
PATH=$ORACLE_HOME/bin:$BASE_PATH:/etc/init.d; export PATH


#verifica os bancos que estao ativos
for bancos in `cat /etc/oratab|grep -v '^#'|sed '/^$/d'|cut -f1 -d: -s|grep -vi 'asm'`
do


 # verifica se o banco esta no ar e pega a instancia corrente
 for instanciaCorrente in `ps -ef|grep pmon|grep oracle|grep ${bancos}|grep -v grep | awk '{print $8}'| sed 's/ora_pmon_//'`
 do

   # se estiver no ar ... then (tamanho nao zero)
   if [ -n "$instanciaCorrente" ]; then


        #lsnrctl status|sed 's/^[ \t]*//'|sed 's/[ \t]*$//'|grep -E '^[SI]'|grep -B 1 admd1|grep SER| awk '{print $2}' | sed 's/"//g'

         #lista os servico da instancia corrente
         for servico in `lsnrctl status|sed 's/^[ \t]*//'|sed 's/[ \t]*$//'|grep -E '^[SI]'|grep -B 1 ${instanciaCorrente}|grep SER| awk '{print $2}' | sed 's/"//g'`
         do

                #pega a instancia preferencial
                InstanciaPreferencial=`srvctl config service -d ${bancos} -s ${servico} | grep Preferred |awk '{print $3}'`


                        #compara se a instancia corrente nÃ£o Ã© a mesma que a preferencial
                        if [ "$instanciaCorrente" != "$InstanciaPreferencial" ]; then


                                #pega a situacao da instancia preferencial
                                instanciaPrefSituacao=`srvctl status database -d ${bancos} |grep "$InstanciaPreferencial"`

                                #verifica se a instancia preferencial esta ativa
                                if [[ "$instanciaPrefSituacao" == *"is running"* ]]; then

                                        #pega a instancia preferencial
                                        instanciaPref=`echo $instanciaPrefSituacao | awk '{print $2}' | sed 's/"//g'`

                                        # reloca o servico para sua instancia preferencial
                                        srvctl relocate service -d ${bancos} -s "$servico" -i "$instanciaCorrente" -t "$instanciaPref"

                                        # log
                                        echo "relocate: "$servico"   "$instanciaCorrente" -> "$instanciaPref"   `date "+%d/%m/%Y %H:%M:%S"`" >> /log_directorie/${bancos}_log_relocate.txt


                                fi
                        fi
         done

        fi

 #instancia
 done


#bancos
done

exit
